---
title: RGPD
stylesheet: pages/plain_md
---

# Politique de confidentialité

FLAP et le Client s’engagent à respecter la réglementation en vigueur applicable au traitement de données à caractère personnel et, en particulier, le règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 applicable à compter du 25 mai 2018, dite RGPD.

La présente section a pour objet de définir les conditions dans lesquelles FLAP, sous-traitant, s’engage à effectuer pour le compte du Client, responsable de traitement, les opérations de traitement de données à caractère personnel.

## Description du traitement faisant l’objet de la sous-traitance

FLAP est autorisé à traiter pour le compte du responsable de traitement les données à caractère personnel nécessaires pour fournir, à la demande du Client, les services ci-après.

### Hébergement ou infogérance d’applications

La nature des opérations réalisées sur les données est l’hébergement ou l’infogérance des applications auxquelles le Client à souscrit.

Dans de nombreux cas, FLAP ignore, conformément à l’article 6-1-2 de la loi n°2004-575 du 21 juin 2004, une ou plusieurs finalités poursuivies par le Client, le type de données à caractères personnels concernées et les catégories de personnes concernées.

En revanche, chaque application a une ou plusieurs finalités intrinsèques décrites sur <https://www.flap.cloud>.

Pour chaque utilisateurs, le système de gestion de compte contient les données suivants :

- Pseudonyme
- Nom d'affichage
- Adresse mail
- Empreinte de mot de passe

Dans certains services, l’utilisateur peut compléter avec d’autres données à caractères personnels le concernant.

Les catégories de personnes concernées sont en général les usagers, qu’ils possèdent un compte ou soient simples visiteurs des services. Il reste possible que soient importées dans ces outils des données à caractère personnel concernant d’autres personnes, qui ne sont pas utilisatrices.

### Support et conseil

Lorsque le Client demande de l’aide ou signale un dysfonctionnement, le Client peut être amené à porter à la connaissance de FLAP, volontairement ou non, des données à caractère personnel. Par exemple, ceci peut arriver lors d’un partage d’écran. Dans ce cadre, les données à caractère personnel concernées par le traitement ainsi que les catégories de personnes concernées ne sont pas connues à l’avance. FLAP et le Client, compte tenu de leurs possibilités et du contexte, font en sorte de limiter le plus possible l’étendue de ce partage d’informations.

S'il s’avère que la demande de support ou de conseil concerne manifestement un traitement de données à caractère personnel de grande étendue, les parties documentent les modalités de traitement de ces données.

### Développement logiciel et autres services

Si la prestation de développement implique un traitement de données à caractère personnel, ce traitement fait l’objet d’une description spécifique incluse dans le Devis et décrivant la nature des opérations, la finalité du traitement, les données à caractère personnel concernées et les catégories de personnes concernées.

## Obligations de FLAP vis-à-vis du responsable de traitement

FLAP s'engage à :

1. Traiter les données uniquement pour la ou les seule(s) finalité(s) qui fait/font l’objet de la sous-traitance.
2. Traiter les données conformément aux instructions documentées du responsable de traitement et transmises à FLAP. Si FLAP considère qu’une instruction constitue une violation du règlement européen sur la protection des données ou de toute autre disposition du droit de l’Union ou du droit des États membres relative à la protection des données, il en informe immédiatement le responsable de traitement. En outre, si FLAP est tenu de procéder à un transfert de données vers un pays tiers ou à une organisation internationale, en vertu du droit de l’Union ou du droit de l’État membre auquel il est soumis, il doit informer le responsable du traitement de cette obligation juridique avant le traitement, sauf si le droit concerné interdit une telle information pour des motifs importants d'intérêt public
3. Garantir la confidentialité des données à caractère personnel traitées dans le cadre de l’exécution de la commande.
4. Veiller à ce que les personnes autorisées à traiter les données à caractère personnel dans le cadre de la réalisation du service :
    - S’engagent à respecter la confidentialité ou soient soumises à une obligation légale appropriée de confidentialité.
    - Reçoivent la formation nécessaire en matière de protection des données à caractère personnel.
5. Prendre en compte, s’agissant de ses outils, produits, applications ou services, les principes de protection des données dès la conception et de protection des données par défaut.

## Sous-traitance

Dans le cadre de l’hébergement d’applications ou de serveur, ou de l’infogérance de serveurs loués au nom de FLAP, FLAP est autorisé à faire appel, en tant que sous-traitant ultérieur, à l’une des entités définies dans la liste des fournisseurs de serveur informatique compatibles avec les services de FLAP.

En cas de migration vers les services d’un autre sous-traitant ultérieur de la liste, FLAP informe préalablement et par écrit le responsable de traitement du changement envisagé. Le responsable de traitement dispose d’un délai minium de 15 jours à compter de la date de réception de cette information pour présenter ses objections. La migration ne peut être effectuée que si le responsable de traitement n'a pas émis d'objection pendant le délai convenu. Ce délai n’existe pas si le Client est à l’origine de la demande de migration.

En cas de recrutement de sous-traitants ultérieurs dans d'autres circonstances, le sous-traitant doit recueillir l’autorisation écrite, préalable et spécifique du responsable de traitement.

## Droit d’information des personnes concernées

Il appartient au responsable de traitement de fournir l’information aux personnes concernées par les opérations de traitement au moment de la collecte des données.

## Exercice des droits des personnes

Dans la mesure du possible, FLAP doit aider le responsable de traitement à s’acquitter de son obligation de donner suite aux demandes d’exercice des droits des personnes concernées : droit d’accès, de rectification, d’effacement et d’opposition, droit à la limitation du traitement, droit à la portabilité des données, droit de ne pas faire l’objet d’une décision individuelle automatisée (y compris le profilage).

Lorsque les personnes concernées exercent auprès de FLAP des demandes d’exercice de leurs droits, FLAP doit adresser ces demandes dès réception par courrier électronique à l’adresse communiquée lors de la souscription des services.

## Notification des violations de données à caractère personnel

FLAP notifie au responsable de traitement toute violation de données à caractère personnel dans un délai maximum de 24 heures après en avoir pris connaissance par courrier électronique à l’adresse communiquée lors de la souscription des services. Cette notification est accompagnée de toute documentation utile afin de permettre au responsable de traitement, si nécessaire, de notifier cette violation à l’autorité de contrôle compétente.

Le Client assume la communication auprès des personnes concernées des violations des données à caractère personnel.

## Aide de FLAP dans le cadre du respect par le responsable de traitement de ses obligations

FLAP aide le responsable de traitement pour la réalisation d’analyses d’impact relative à la protection des données.

FLAP aide le responsable de traitement pour la réalisation de la consultation préalable de l’autorité de contrôle.

## Mesures de sécurité

Les postes de travail utilisés par FLAP sont chiffrés ainsi que ses unités de sauvegarde et les sauvegardes elles-mêmes.

Dans le cadre des offres d’hébergement ou d’infogérance, FLAP s’engage à mettre en œuvre les mesures de sécurité suivantes : contrôle des signatures serveurs, authentification par clé, chiffrement TLS, chiffrement des sauvegardes, mises à jour régulières tels que définies dans les présentes, mise en place d’un parefeu et d’un programme contre les attaques par force brute, monitoring des services, suivi des failles de sécurité, vérification régulière de l’effectivité des sauvegardes.

Lors des prestations de développement, FLAP applique de son mieux les bonnes pratiques en matières de sécurité.

Pour le conseil et le support, FLAP limite le nombre d’interlocuteur afin d’éviter les attaques par ingénierie sociale.

Par ailleurs, FLAP conseille ses Clients pour sécuriser le transfert des informations avec FLAP.

## Devenir des données

Au terme de la prestation de service relatif au traitement des données à caractère personnel, FLAP s’engage à transmettre une copie de toutes ces données au responsable de traitement (par exemple par la transmission de la sauvegarde dans le cas de l’hébergement ou l’infogérance) puis à les détruire.

## Délégué à la protection des données

FLAP communique au responsable de traitement le nom et les coordonnées de son délégué à la protection des données, s’il en a désigné un conformément à l’article 37 du règlement européen sur la protection des données

## Registre des catégories d’activités de traitement

FLAP déclare tenir par écrit un registre de toutes les catégories d’activités de traitement effectuées pour le compte du responsable de traitement comprenant :

- le nom et les coordonnées du responsable de traitement pour le compte duquel il agit, des éventuels sous-traitants et, le cas échéant, du délégué à la protection des données
- les catégories de traitements effectués pour le compte du responsable du traitement
- le cas échéant, les transferts de données à caractère personnel vers un pays tiers ou à une organisation internationale, y compris l'identification de ce pays tiers ou de cette organisation internationale et, dans le cas des transferts visés à l'article 49, paragraphe 1, deuxième alinéa du règlement européen sur la protection des données, les documents attestant de l'existence de garanties appropriées
- dans la mesure du possible, une description générale des mesures de sécurité techniques et organisationnelles, y compris entre autres, selon les besoins :
  - la pseudonymisation et le chiffrement des données à caractère personnel
  - des moyens permettant de garantir la confidentialité, l'intégrité, la disponibilité et la résilience constantes des systèmes et des services de traitement
  - des moyens permettant de rétablir la disponibilité des données à caractère personnel et l'accès à celles-ci dans des délais appropriés en cas d'incident physique ou technique
  - une procédure visant à tester, à analyser et à évaluer régulièrement l'efficacité des mesures techniques et organisationnelles pour assurer la sécurité du traitement.

## Documentation

FLAP met à la disposition du responsable de traitement la documentation nécessaire pour démontrer le respect de toutes ses obligations et pour permettre la réalisation d'audits, y compris des inspections, par le responsable du traitement ou un autre auditeur qu'il a mandaté, et contribuer à ces audits.

## Obligations du responsable de traitement vis-à-vis du sous-traitant

Le responsable de traitement s’engage à :

1. documenter par écrit toute instruction concernant le traitement des données par FLAP
2. veiller, au préalable et pendant toute la durée du traitement, au respect des obligations prévues par le règlement européen sur la protection des données de la part du sous-traitant
3. superviser le traitement conformément aux présentes.
