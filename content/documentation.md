---
title: Informations techniques
stylesheet: pages/plain_md
---

# Informations techniques

## Infrastructure

### Matérielle

Chaque Client bénéficie d'un VPS ou d'un serveur dédié qui lui est attribué. Ce serveur est hébergé par la société OVH.

Le choix des capacités du serveur est fait selon les besoins du Client.

Les sauvegardes sont stockées, chiffrées, sur les serveurs de stockage de Scaleway.

#### Niveau de contrôle de FLAP

FLAP a un accès administrateur sur chaque serveur. Cette accès se fait grâce au logiciel SSH, il est sécurisé par une clé d'identification.

En cas de problème matériel ou réseau, FLAP peut contacter le support.

Les VPS OVH sont des VPS KVM, vu la taille de l'hébergeur les VPS font plus que probablement appelle à des disques virtuelles issues de baies de stockage.

Concernant la confidentialité des données. FLAP ne sait pas dans quel mesures il est facile pour les administrateurs systèmes OVH d'accéder aux données dans les disques.

En cas de données sensibles il est toujours recommandé de les chiffrer avant de les stocker sur quelques serveurs que ce soit.

FLAP est dans l'incapacité de déterminer les microcodes utilisés sur les hyperviseurs. Il y a donc probablement des microcodes propriétaires.

### Logiciel

Pour administrer les services, FLAP utilise son propre logiciel d'administration, diffusé sous licence libre GPL-v3.

FLAP utilise uniquement des logiciels eux aussi publiés sous licence libres.

FLAP utilise la dernière version de la distribution linux Debian.

#### Niveau de contrôle de FLAP

FLAP détient une maîtrise totale sur son logiciel d'administration lui permettant de l'adapter aux besoins de ses Clients.

En revanche, FLAP n'a qu'un contrôle limité sur les services utilisés. FLAP peut:

- suivre en détail les mises à jours de chaque services.
- effectuer, en de rares occasions, des modifications afin d'adapter le service à son offre.

## Sauvegardes

Une sauvegarde chiffrée des données est réalisée deux fois par jours par FLAP. Elles sont stockées sur un serveur de stockage distinct chez l'hébergeur Scaleway. Sous demande, le Client peut être informé de l’emplacement géographique de ses sauvegardes.

Le Client peut accéder sur demande à ses sauvegardes dans un format qui contient l'ensemble de ses données. Ces sauvegardes lui permettent d'héberger ses applications ailleurs.

FLAP conserve pour chaque Clients les sauvegardes suivantes:

- 1 par jour sur les 7 derniers jours.
- 1 par semaine sur les 5 dernières semaines.
- 1 par mois sur les 12 derniers mois.

## Rétention des logs

Les logs du système et des services sont automatiquement supprimés au bout d'un an.
