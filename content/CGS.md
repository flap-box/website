---
title: CGS
stylesheet: pages/plain_md
---

# Conditions Générales de Service

**Objet du document:** définir les modalités de mise à disposition des services proposés par FLAP.

## Évolution des conditions générales de service

FLAP se réserve le droit de mettre à jour et modifier ces conditions. Dans ce cas, FLAP informe les clients concernés.

## Modalité de commandes

Le Client peut commander des services via les formulaires du site [www.flap.cloud](https://www.flap.cloud) ou par mail. FLAP peut demander des précisions avant d’acter la validation d’une commande.

Toute commande concernant une prestation incluant des éléments non présentés sur [www.flap.cloud](https://www.flap.cloud) fait l’objet d’un Devis personnalisé. Lorsqu’un Devis personnalisé adressé au Client complète les présentes conditions générales, les conditions exprimées dans le Devis prévalent.

Lors de la commande, FLAP peut demander la signature par le Client des conditions générales et de l’éventuel Devis.

## Respect de la Charte du collectif C.H.A.T.O.N.S.

Dans le cadre de son activité d’hébergement d’applications libres, FLAP s’engage à respecter la charte du Collectif des Hébergeurs, Alternatifs, Transparents, Ouverts, Neutres et Solidaires. FLAP est candidat pour intégrer ce collectif.

Conformément à cette charte, FLAP s’engage notamment à:

- Ne pas mettre en place de dispositif de suivi des usages des services proposés autres qu’à des fins statistiques ou administratives.
- Ne pas mettre en place de publicité sur ses outils.
- Ne s’arroger aucun droit de propriété sur les contenus, données et métadonnées produits par les usagers.
- Utiliser exclusivement des logiciels soumis à des licences libres.
- Ne pratiquer aucune surveillance des actions des utilisateurs et utilisatrices, autre qu’à des fins administratives, techniques, ou d’améliorations internes des services.

L’ensemble des applications proposées sur [www.flap.cloud](https://www.flap.cloud) ainsi que l'outil interne d'administration sont sous licences libres.

## Contenu possédé par le Client

FLAP héberge, au travers de certaines applications, du contenu qui n'est pas sous licence libre et dont le Client possède les droits de propriété intellectuelle. Dans ce cas, le service proposé par FLAP respecte bien la charte. En revanche, en aucun cas, le Client ne peut laisser croire aux visiteurs de son site:

- que son service respecte la charte CHATONS puisque le site n’est pas sous licence libre et que le Client n’a pas intégré le collectif
- que FLAP en est l’éditeur ou en possède la propriété intellectuelle.

## Infrastructure

FLAP n’intervient que sur des serveurs situés en France et hébergés par les fournisseurs de serveurs informatiques OVH et Scaleway.

Sur demande du Client, FLAP peut informer le Client des localisations géographiques de ses données.

## Maintenance

FLAP assure la maintenance des services installés, sauf si le Client a choisi de réaliser cette maintenance lui même. Dans ce cas, FLAP met à disposition du Client un outil de suivi de l’état de ses services.

### Mise à jour des applications

FLAP assure la mise à jour régulière du système et des applications.

En général, les opérations de maintenance ont lieu hors jours ouvrés ou entre 20h et 6h.

### Veille de sécurité

FLAP assure une veille concernant les éventuelles failles de sécurité et instabilités de ses systèmes. FLAP met en œuvre tout correctif jugé nécessaire, par tout moyen.

### Intervention en cas de panne

En cas de panne constatée et si aucun message n’atteste sur la page de statut que FLAP est en train de corriger le dysfonctionnement, le Client doit faire un signalement en appelant au 06 22 29 03 82. En cas d’indisponibilité de ce numéro, le Client doit laisser un message et envoyer un mail.

Dès réception d’un signalement, FLAP intervient le plus promptement possible compte tenu du contexte (nombre d’usagers impactés, gravité, temps nécessaire pour résoudre le problème, disponibilité, etc).

## Sauvegarde & restauration

Une sauvegarde chiffrée des données est réalisée deux fois par jours par FLAP. Elles sont stockées sur un serveur de stockage distinct chez l'hébergeur Scaleway. Sous demande, le Client peut être informé de l’emplacement géographique de ses sauvegardes.

Le Client peut accéder sur demande à ses sauvegardes dans un format qui contient l'ensemble de ses données. Ces sauvegardes lui permettent d'héberger ses applications ailleurs.

FLAP conserve pour chaque Clients les sauvegardes suivantes:

- 1 par jour sur les 7 derniers jours.
- 1 par semaine sur les 5 dernières semaines.
- 1 par mois sur les 12 derniers mois.

## Support et conseil

Le Client nomme jusqu’à cinq personnes habilitées à communiquer avec FLAP, dans le cadre de l’utilisation des services.

FLAP se réserve le droit de facturer le temps de support et de conseil téléphonique dépassant l’offre initiale.

## Accès administrateur

Le Client obtient un accès administrateur lui permettant de gérer en toute indépendance ses comptes utilisateurs et noms de domaine.

Pour certaines applications, le Client obtient un accès administrateur lui permettant des réglages plus fins. Il est responsable des éventuelles pannes causées par un mésusage de cet accès administrateur. Il doit conserver ses identifiants et mots de passe de façon sécurisée, notamment en choisissant un mot de passe suffisamment robuste et en le conservant de façon chiffrée ou manuscrite dans un lieu sûr.

## Mésusage des services

Le Client doit respecter les lois et réglementations en vigueur lors de l’usage des services proposés que ce soit en matière de respect de la vie privée, d’envoi de mails en grande quantité, de propriété intellectuelle, de propos discriminatoires, d’appel à la haine, de harcèlement, d’atteinte aux libertés fondamentales de personnes, etc.

En cas d’usage prohibé, FLAP peut se trouver dans l’obligation de déclencher la suspension totale ou partielle du service, le retrait de contenu, ou toute autre mesure que les lois et réglementations lui imposent.

## Délais de livraison

FLAP met en service les applications commandées sous 10 jours à partir de la réception des éléments nécessaires à leur déploiement. Ce délai ne s’applique pas lors des congés annuels notifiés sur le site.

## Propriété intellectuelle

Le service proposé est composé de logiciels libres. Si des développements sont nécessaires pour réaliser cette mission, ceux-ci seront publiés sous la licence libre ou open-source qui semblera appropriée.

Le Client et ses usagers conservent leurs droits d’auteur et de marque sur toutes les données (fichiers, contacts, logos, etc.) importées au sein de ces logiciels.

## Transmission sécurisée d’identifiants

FLAP ne demandera jamais la transmission d’identifiants autrement que par le formulaire de contact de [www.flap.cloud](https://www.flap.cloud), lequel est suffisamment sécurisé pour cet usage. Le Client doit s’assurer d’être sur le bon site lorsqu'il effectue cette démarche. En cas de doute, il doit appeler au 06 22 29 03 82.

## Conditions financières

### Tarifs

Sauf mentions contraires, tous les prix sont mentionnés en euros. FLAP est en franchise de TVA (article 293B du CGI).

Pour les prestations d’hébergement, les prix sont consultables à la page "Prix" du site web [www.flap.cloud](https://www.flap.cloud).

### Facturation

Les abonnements sont tous renouvelables par tacite reconduction, et facturés en début de période.

Le Client choisi une facturation:

- mensuelle: facturation au début de mois
- annuelle: facturation au début de l’année civile. Ce choix donne droit à une remise de 10% sur le prix total.

La première facture d’un abonnement est établie au pro rata du nombre de jours restant avant la fin de la période en cours.

### Retard de paiement

Les factures sont payables sous 15 jours nets, sans escompte à partir de la date de facturation.

En cas de retard, une indemnité forfaitaire de 40 € s’applique. Le taux des pénalités de retard est égal à 12%.

## Résiliation

Le Client peut choisir de mettre fin à un abonnement. Il indique dans ce cas une date de résiliation postérieure à la période déjà facturée. La dernière facture est établie au pro-rata du nombre de jours non encore facturés.

En cas d’inexécution ou de mauvaise exécution des obligations de l’une des parties, il sera procédé à l’envoi d’une mise en demeure par courrier recommandé avec accusé de réception notifiant le manquement. Si dans un délai de 15 jours à compter de la réception de la mise en demeure aucune solution n’est trouvée par les parties, il sera procédé à la résiliation de la prestation.

En cas de résiliation, FLAP transmet la dernière sauvegarde des applications puis procède à la destruction des données.

## Périmètre géographique

Les présentes conditions s’appliquent dans le cadre de collaboration (vente et utilisation) avec des personnes physiques ou morales françaises ou résidentes en France, ceci afin de simplifier le cadre juridique et administratif dans lequel évolue FLAP.

Si le Client utilise ou autorise des tiers à utiliser les services fournis en dehors de cette zone géographique, il en porte l’entière responsabilité.

## Responsabilité

FLAP est assujetti à une obligation de moyens, considérant la haute technicité des technologies mises en œuvre. En cas de défaillance, FLAP ne peut être tenu pour responsable des dommages indirects tels que pertes d’exploitation, préjudices commerciaux, perte de Clientèle, de chiffre d’affaires, de bénéfices ou d’économies prévus, ou de tout autre préjudice indirect.

Une fois le système de sauvegarde en place, FLAP ne pourra pas être tenu responsable pour:

- la perte de données des dernières 24h induites par la restauration d’une sauvegarde
- la perte ou divulgation de données du fait d’une erreur, d’une panne, d’une attaque ou de plusieurs de ces faits à la fois, survenant simultanément sur ou contre le serveur et/ou l’une des deux sauvegardes

La responsabilité de FLAP ne saurait être engagée en cas de retards ou d’inexécutions résultant d’un cas de force majeure, telle que reconnue par la jurisprudence des tribunaux français. Sont également considérés comme des cas de force majeure au titre des présentes:

- le blocage ou l’interruption des réseaux de télécommunications,
- l’absence ou la suspension de la fourniture d’électricité par le fournisseur historique,
- les catastrophes naturelles,
- l’arrêt maladie de plus de 2 semaines du Président de FLAP,
  et tout autre cas indépendant de la volonté du Prestataire et empêchant l’exécution normale de la prestation.

## Données personnelles et respect de la vie privée

FLAP ne vend aucune donnée personnelle à qui que ce soit.

FLAP porte une attention particulière au respect de la vie privée, notamment en évitant d’incorporer des technologies de pistages.

Les outils de statistiques que FLAP peut mettre en place pour les sites de ses clients ne nécessitent pas le recueil du consentement des visiteurs en conformité avec l’article 32-II de la loi du 6 janvier 1978, modifié par l’ordonnance n°2011-1012 du 24 août 2011 (transposé dans la directive 2009/136/CE). Les statistiques sont anonymisées, et ne recueillent pas d’information géographique plus précise que le pays.

L’absence de pistage dans les offres de FLAP, n’implique pas forcément que les sites internet des clients de FLAP hébergés n’en contiennent pas. Les clients de FLAP sont seuls responsables de leurs sites internet.

FLAP n’utilise ni ne consulte les données confiées ou générées par ses clients et leurs utilisateurs, sauf à des fins de réalisation du service demandé par le Client. Voir Dispositions relatives au respect du RGPD pour plus de précisions à ce propos.

## Litige et juridiction compétente

Le droit applicable aux présentes est le droit français. En cas de différent, les parties recherchent une solution amiable. Si la démarche échoue, le litige sera tranché par le Tribunal de Grande Instance d'Avignon.

Le fait que le Client ou FLAP ne se prévale pas à un moment donné de l’une des présentes conditions générales et/ou tolère un manquement par l’autre partie ne peut être interprété comme valant renonciation par le Client ou FLAP à se prévaloir ultérieurement de ces conditions.

La nullité d’une des clauses de ces conditions en application d’une loi, d’une réglementation ou d’une décision de justice n’implique pas la nullité de l’ensemble des autres clauses. Par ailleurs l’esprit général de la clause sera à conserver en accord avec le droit applicable.

## Mentions légales

Conformément à la loi du 21 juin 2004:

Ce site internet est édité et hébergé par la société FLAP S.A.S. à capital de 500€, inscrite au Registre du Commerce et des Sociétés d'Avignon sous le n° 853 009 058. En conséquence, vous êtes libre de contacter la société FLAP SAS par téléphone pour signaler toutes infractions stipulées dans l’article 6.I.7 de la loi pour la Confiance dans l’Économie Numérique du 21 juin 2004.

Le siège social est situé:
702 petite route de Cadenet, 84360 Lauris, Tél. 06 22 29 03 82, Email louis@chmn.me

Le directeur de la publication de ce service Internet est M. Louis CHEMINEAU.

Ce site a été conçu à partir de ressources sous licences libres ou issues du domaine publique, certains éléments sont cependant la propriété exclusive de la société FLAP S.A.S. ou de Louis CHEMINEAU.

> 11/03/2021
