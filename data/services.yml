services:
  - name: Documents et médias
    id: nextcloud
    icon: file-earmark-text-fill
    description: Grâce à [Nextcloud](https://nextcloud.com), votre FLAP cloud vous permet une gestion simple de vos fichiers.
    image: services/nextcloud.png
    features:
      - title: Sauvegardez en continu vos fichiers
        description: L'application _Nextcloud_ sur votre ordinateur vous permet de synchroniser en continu les dossiers de votre choix vers votre FLAP cloud.
        icon: arrow-repeat
        color: blue
        shape: round
      - title: Accédez à vos fichiers
        description: Grâce aux nombreuses applications disponibles pour Android, iOS, Linux, MacOS et Windows, _Nextcloud_ vous permet d’accéder à vos fichiers depuis tous vos appareils.
        icon: folder2-open
        color: yellow
      - title: Partagez et réceptionnez des fichiers
        description: Besoin de partager un fichier trop lourd pour un mail ? Créer simplement un lien de partage !
        icon: share
        color: blue
        shape: round
      - title: Téléversez automatiquement vos photos
        description: Les applications _Nextcloud_ pour Android et iOS vous permettent de sauvegarder automatiquement toutes vos photos et vidéos sur votre FLAP cloud.
        icon: images
        color: yellow
      - title: Collaborez en ligne
        description: Éditez vos fichiers à plusieurs depuis l'interface web de OnlyOffice.
        icon: people
        color: blue
        shape: round
      - title: Remontez le temps
        description: _Nextcloud_ garde en mémoire les anciennes versions de vos fichiers.
        icon: arrow-counterclockwise
        color: yellow

  - name: Mails, calendriers et contacts
    id: sogo
    icon: envelope
    description: "[SOGo](https://sogo.nu) vous propose une interface web unifiée pour vos mails, calendriers et contacts."
    image: services/sogo.png
    features:
      - title: Gérez vos calendriers et tâches
        description: Stockez et gérez vos calendriers grâce à l'interface de _SOGo_. Vous pouvez même partager des calendriers au sein de votre FLAP cloud afin de faciliter votre quotidien. Par exemple partagez une liste de courses ou bien vos horaires de travail.
        icon: calendar3
        color: blue
        shape: round
      - title: Gérez vos contacts
        description: Stockez et gérez vos contacts sur votre FLAP cloud.
        icon: person-lines-fill
        color: yellow
      - title: Recevez vos mails chez vous
        description: Votre FLAP cloud vous permet de recevoir des mails sur le nom de domaine que vous lui avez associé. Vos mails arrivent directement chez vous, comme votre courrier papier.
        icon: envelope
        color: blue
        shape: round
      - title: Synchronisez vos calendriers et contacts
        description: Votre FLAP cloud vous permet de synchroniser facilement vos calendriers et contacts sur tous vos appareils, en s'intégrant avec les applications que vous utilisez déjà.
        icon: arrow-repeat
        color: yellow

  - name: Messagerie instantanée
    id: element
    icon: chat-text
    description: Discutez avec vos collaborateurs grâce à [Element](https://element.io) et [Matrix](https://matrix.org).
    image: services/element.png
    features:
      - title: Conversez avec textes, images et vidéos
        description: Discutez avec les membres de votre FLAP à partir de tous vos appareils grâce aux interfaces web, bureau et mobile.
        icon: chat-text
        color: blue
        shape: round
      - title: Passez des appels audio et vidéo
        description: Le texte ne suffit pas ? Passez des appels audio et vidéo en toute simplicité.
        icon: telephone-outbound
        color: yellow
      - title: Rester connecter aux membres extérieurs
        description: _Element_ s'appuie sur le protocole Matrix pour fonctionner. Ce dernier vous permet de communiquer avec les utilisateurs d'autres fournisseurs de services compatibles avec le protocole Matrix.
        icon: share
        color: blue
        shape: round
      - title: Créez des Conversations de groupe
        description: Vous pouvez créer différents groupes de conversation pour vous organiser.
        icon: people
        color: yellow
      - title: Communiquez à l'international
        description: Peu importe votre destination, vos messages ne seront pas bloqués par les décisions politiques locales.
        icon: map-fill
        color: blue
        shape: round
      - title: Rejoignez des discussions publiques
        description: De nombreux projets utilisent _Element_ de manière publique pour s'organiser. Vous pouvez alors les rejoindre et prendre part à la discussion.
        icon: globe
        color: yellow

  - name: Visioconférence
    id: jitsi
    icon: camera-video
    description: Une réunion à distance, besoin de se voir et de discuter ? Pas de problème avec votre instance de [Jitsi](https://jitsi.org).
    image: services/jitsi.png
    features:
      - title: Créez un nombre illimité de conférences
        description: Besoin de créer plusieurs conférences en simultané, c'est possible.
        icon: collection
        color: blue
        shape: round
      - title: Partagez votre écran
        description: Une image vaut mille mots, partagez facilement votre écran ou une vidéo Youtube.
        icon: cast
        color: yellow
      - title: Invitez les participants
        description: Collègues ou contacts extérieurs, un simple lien suffit pour inviter les différents participants. Ils n'auront d'ailleurs rien à installer.
        icon: person-plus
        color: blue
        shape: round
      - title: Limitez l'accès avec un mot de passe
        description: Votre visioconférence risque d'être perturbée ? Sécurisez là grâce à un mot de passe.
        icon: shield-lock
        color: yellow

  - name: Audience de sites Web
    id: matomo
    icon: graph-up
    description: Analyser l'audience de votre site web avec [Matomo](https://matomo.org).
    image: services/matomo.png
    features:
      - title: Analyser le trafic de vos sites internet.
        description: _Matomo_ vos permet de comprendre comment les visiteurs naviguent sur vos sites internet.
        icon: graph-up
        color: blue
        shape: round
      - title: Retirez la bannière cookie de vos sites.
        description: En utilisant _Matomo_ vous ne partagez plus la responsabilité de traitement de données récupérées via les cookies. Il vous suffit de respecter de simples conditions pour retirer cette ennuyeuse bannière de consentement.
        icon: pip-fill
        color: yellow

  - name: Diffusion de vidéos
    id: peertube
    icon: collection-play-fill
    description: "[Peertube](https://joinpeertube.org), c'est Youtube, mais en mieux."
    image: services/peertube.png
    features:
      - title: Publiez vos vidéos.
        description: Besoin d'un espace sous votre contrôle pour diffuser vos vidéos ? La réponse est _Peertube_ !
        icon: collection-play-fill
        color: blue
        shape: round
      - title: Pas de publicités.
        description: _Peertube_ n'ajoute pas de publicités au début de vos vidéos, vos auditeurs vous remercieront !
        icon: emoji-heart-eyes-fill
        color: yellow
      - title: Fédérez vous avec ActivityPub.
        description: _Peertube_ utilise le protocole ActivityPub lui permettant de communiquer avec un réseau grandissant de plateformes réunissant plusieurs millions d'utilisateurs.
        icon: globe2
        color: blue
        shape: round
      - title: Échappez à la censure.
        description: Avec _Peertube_, pas d'algorithme à satisfaire, ou d'obscure règles de publication à respecter. Vos vidéos, vos règles.
        icon: shield-slash-fill
        color: yellow

  - name: Diffusion de musiques
    id: funkwhale
    icon: file-music-fill
    description: "[Funkwhale](https://funkwhale.audio), c'est SoundCloud, mais en mieux."
    image: services/funkwhale.jpg
    features:
      - title: Publiez vos musiques.
        description: Besoin d'un espace sous votre contrôle pour diffuser vos musique ? La réponse est _Funkwhale_ !
        icon: music-note-list
        color: blue
        shape: round
      - title: Pas de publicités.
        description: _Funkwhale_ n'ajoute pas de publicités au début de vos musiques, vos auditeurs vous remercieront !
        icon: emoji-heart-eyes-fill
        color: yellow
      - title: Fédérez vous avec ActivityPub.
        description: _Funkwhale_ utilise le protocole ActivityPub lui permettant de communiquer avec un réseau grandissant de plateformes réunissant plusieurs millions d'utilisateurs.
        icon: globe2
        color: blue
        shape: round
      - title: Échappez à la censure.
        description: Avec _Funkwhale_, pas d'algorithme à satisfaire, ou d'obscure règles de publication à respecter. Vos musiques, vos règles.
        icon: shield-slash-fill
        color: yellow
